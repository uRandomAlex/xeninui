## The Xenin Framework
A framework developed for Garry's Mod, intended to make mundane tasks just a bit easier and more interconnected.

### Includes
* SQL query builder
* SQL migrations
* MySQL/SQLite support (MySQLite)
* Tons of pre-built UI panels that follows a specific theme
* Lots of helper functions
* Ability to connect Xenin scripts together using the common framework interface

### Docs
Documentation is coming soonTM